#include <iostream>
#include <stdio.h>
#include <chrono>
#include "image.h"
using namespace std::chrono;
using tp_t = high_resolution_clock::time_point;

void gaussianKernel(float* in, float* out, int width, int height) {
	float gaussFilter[25] = {
		0.000, 0.002, 0.005, 0.002, 0.000,
		0.002, 0.042, 0.117, 0.042, 0.002,
		0.005, 0.117, 0.325, 0.117, 0.005,
		0.002, 0.042, 0.117, 0.042, 0.002,
		0.000, 0.002, 0.005, 0.002, 0.000
	};

	for (int y = 0; y < height; ++y) {
		for (int x = 0; x < width; ++x) {
			int ix = x - 2;
			int iy = y - 2;
			double sum = 0;
			for (int j = 0; j < 5; ++j) {
				int f_offset = j * 5;
				int yy = iy + j;
				yy = yy < 0 ? 0 : (yy >= height ? height - 1 : yy);
				int in_offset = yy * width;
				for (int i = 0; i < 5; i++) {
					int xx = ix + i;
					xx = xx < 0 ? 0 : (xx >= width ? width - 1 : xx);
					sum += gaussFilter[f_offset + i] * in[in_offset + xx];
				}
			}
			out[y * width + x] = sum;
		}
	}
}

int main( int argc, char** argv)
{
	char *InFile = "testImage.bmp";
	char *OutFile = "outImageC.bmp";

	int width = 1280;
	int height = 720;
	int size = width * height;

	float* inputImage = new float[size];
	float* outputImage = new float[size];

	// Read image on CPU
	bmp_read(InFile, inputImage, width, height);

	int t1 = 0, t2 = 0;
	tp_t t1_s = high_resolution_clock::now();
	// Kernel execution
	gaussianKernel(inputImage, outputImage, width, height);
	tp_t t1_e = high_resolution_clock::now();
	t1 += (int) duration_cast<microseconds>(t1_e - t1_s).count();
	printf("%d\n", t1);
	// Write image on CPU
	bmp_write(OutFile, outputImage, width, height);
	delete[] inputImage;
	delete[] outputImage;

	return 0;
}
