#!/bin/bash
rm filter
nvcc -c image.cpp gaussianFilter.cu -std=c++11
nvcc -o filter image.o gaussianFilter.o
rm image.o gaussianFilter.o
