#!/bin/bash
rm cfilter
nvcc -c image.cpp gaussianFilterCPU.cpp -std=c++11
nvcc -o cfilter image.o gaussianFilterCPU.o
rm image.o gaussianFilterCPU.o
