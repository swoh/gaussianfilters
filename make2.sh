#!/bin/bash
g++ -c image.cpp
nvcc -c gaussianFilterShared.cu -std=c++11
nvcc -o sfilter image.o gaussianFilterShared.o
rm image.o gaussianFilterShared.o
