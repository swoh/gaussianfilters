#include "image.h"

char bmp_gray_header[1078];

void bmp_read(const char InputFile[], float read_data[], int width, int height) {
    FILE *fp;

    if ((fp = fopen(InputFile, "rb")) == NULL) {
        printf("Can not open BMP file: %s\n", InputFile);
        return ;
    }

    fread(bmp_gray_header, sizeof(char), 1078, fp);
    
	for(int row = 0; row < height; row++) {
		for(int col = 0; col < width; col++) {
			read_data[width*row + col] = (float)fgetc(fp);
        }
	}

    fclose(fp);
    
	return ;
}

void bmp_write(const char OutputFile[], float write_data[], int width, int height) {
	FILE *fp;

    if ((fp = fopen(OutputFile, "wb")) == NULL) {
        printf("Can not create BMP file: %s\n", OutputFile);
    }
    
    fwrite((char *)(bmp_gray_header), 1, 1078, fp);

    for(int row = 0; row < height; row++) { 
		for(int col = 0; col < width; col++) {
			fprintf(fp,"%c", (unsigned char)write_data[width*row + col]);	
        }
	}

	fclose(fp);    

	return;
}
