#include <iostream>
#include <stdio.h>
#include <chrono>
#include "image.h"
using namespace std::chrono;
using tp_t = high_resolution_clock::time_point;

#define BLOCKS 3600
#define BLOCKS_x 80
#define THREADS 256
#define THREADS_x 16

__global__ void gaussianKernel(float* in, float* out, int width, int height) {
	float gaussFilter[25] = {
		0.000, 0.002, 0.005, 0.002, 0.000,
		0.002, 0.042, 0.117, 0.042, 0.002,
		0.005, 0.117, 0.325, 0.117, 0.005,
		0.002, 0.042, 0.117, 0.042, 0.002,
		0.000, 0.002, 0.005, 0.002, 0.000
	};

	const int bIdx = blockIdx.x;
	const int tIdx = threadIdx.x;
	const int x = (bIdx % 80) * 16 + (tIdx % 16);
	const int y = (bIdx / 80) * 16 + (tIdx / 16);
	int ix = x - 2;
	int iy = y - 2;

	double sum = 0;
	for (int j = 0; j < 5; ++j) {
		int f_offset = j * 5;
		int yy = iy + j;
		yy = yy < 0 ? 0 : (yy >= height ? height - 1 : yy);
		//if (yy >= 0 && yy <= height) {
		int in_offset = yy * width;
		for (int i = 0; i < 5; i++) {
			int xx = ix + i;
			//if (xx >= 0 && xx <= width) {
			xx = xx < 0 ? 0 : (xx >= width ? width - 1 : xx);
			sum += gaussFilter[f_offset + i] * in[in_offset + xx];
			//}
		}
		//}
	}
	out[y * width + x] = sum;
}

int main( int argc, char** argv)
{
	char *InFile = "testImage.bmp";
	char *OutFile = "outImage.bmp";

	int width = 1280;
	int height = 720;
	int size = width * height;

	float* inputImage = new float[size];
	float* outputImage = new float[size];

	// Read image on CPU
	bmp_read(InFile, inputImage, width, height);

	int t1 = 0, t2 = 0;
	tp_t t1_s = high_resolution_clock::now();

	float* dev_in;
	float* dev_out;
	int mem_size = size * sizeof(float);

	cudaMalloc(&dev_in, mem_size);
	cudaMalloc(&dev_out, mem_size);
	// copy the input image from CPU to GPU
	cudaMemcpy(dev_in, inputImage, mem_size, cudaMemcpyHostToDevice);

	// Time start
	tp_t t2_s = high_resolution_clock::now();

	// Kernel execution
	gaussianKernel<<<BLOCKS,THREADS>>>(dev_in, dev_out, width, height);
	//cudaThreadSynchronize();
	cudaDeviceSynchronize();

	// Time end
	tp_t t2_e = high_resolution_clock::now();

	// copy the output image GPU to CPU
	cudaMemcpy(outputImage, dev_out, mem_size, cudaMemcpyDeviceToHost);
	cudaFree(dev_in);
	cudaFree(dev_out);
	tp_t t1_e = high_resolution_clock::now();

	t1 += (int) duration_cast<microseconds>(t1_e - t1_s).count();
	t2 += (int) duration_cast<microseconds>(t2_e - t2_s).count();
	cudaDeviceReset();
	printf("%d, %d\n", t1, t2);
	// Write image on CPU
	bmp_write(OutFile, outputImage, width, height);
	delete[] inputImage;
	delete[] outputImage;

	return 0;
}
