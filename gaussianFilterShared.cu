#include <iostream>
#include <chrono>
#include <stdio.h>
#include "image.h"
using namespace std::chrono;
using tp_t = high_resolution_clock::time_point;

#define BLOCKS 3600
#define BLOCKS_x 80
#define THREADS 256
#define THREADS_x 16

__global__ void gaussianKernel(float* in, float* out, int width, int height) {
	float gaussFilter[25] = {
		0.000, 0.002, 0.005, 0.002, 0.000,
		0.002, 0.042, 0.117, 0.042, 0.002,
		0.005, 0.117, 0.325, 0.117, 0.005,
		0.002, 0.042, 0.117, 0.042, 0.002,
		0.000, 0.002, 0.005, 0.002, 0.000
	};
	__shared__ float local[20][20];

	const int bIdx = blockIdx.x;
	const int tIdx = threadIdx.x;

	const int bx = (bIdx % 80) * 16;
	const int by = (bIdx / 80) * 16;
	const int x = bx + tIdx % 16;
	const int y = by + tIdx / 16;

	if (tIdx < 200) {
		int j = tIdx / 10;
		int i = (tIdx % 10) * 2;
		int yy = by - 2 + j;
		int xx = bx - 2 + i;
		yy = yy < 0 ? 0 : (yy >= height ? height - 1 : yy);
		int in_offset = yy * width;
		for (int k = 0; k <= 1; ++k) {
			int xxx = xx + k;
			xxx = xxx < 0 ? 0 : (xxx >= width ? width - 1 : xxx);
			local[j][i + k] = in[in_offset + xxx];
		}
	}
	__syncthreads();

	const int lx = tIdx % 16;
	const int ly = tIdx / 16;

	double sum = 0;
	for (int j = 0; j < 5; ++j) {
		int f_offset = j * 5;
		int yy = ly + j;
		for (int i = 0; i < 5; i++) {
			sum += gaussFilter[f_offset + i] * local[yy][lx + i];
		}
	}
	out[y * width + x] = sum;
}

int main( int argc, char** argv)
{
	char *InFile = "testImage.bmp";
	char *OutFile = "outImageS.bmp";

	int width = 1280;
	int height = 720;
	int size = width * height;

	float* inputImage = new float[size];
	float* outputImage = new float[size];

	// Read image on CPU
	bmp_read(InFile, inputImage, width, height);

	int t1 = 0, t2 = 0;
	tp_t t1_s = high_resolution_clock::now();

	float* dev_in;
	float* dev_out;
	int mem_size = size * sizeof(float);

	cudaMalloc(&dev_in, mem_size);
	cudaMalloc(&dev_out, mem_size);
	// copy the input image from CPU to GPU
	cudaMemcpy(dev_in, inputImage, mem_size, cudaMemcpyHostToDevice);

	// Time start
	tp_t t2_s = high_resolution_clock::now();

	// Kernel execution
	gaussianKernel<<<BLOCKS,THREADS>>>(dev_in, dev_out, width, height);
	cudaDeviceSynchronize();

	// Time end
	tp_t t2_e = high_resolution_clock::now();

	// copy the output image GPU to CPU
	cudaMemcpy(outputImage, dev_out, mem_size, cudaMemcpyDeviceToHost);
	cudaFree(dev_in);
	cudaFree(dev_out);
	tp_t t1_e = high_resolution_clock::now();

	t1 += (int) duration_cast<microseconds>(t1_e - t1_s).count();
	t2 += (int) duration_cast<microseconds>(t2_e - t2_s).count();
	cudaDeviceReset();
	printf("%d, %d\n", t1, t2);
	// Write image on CPU
	bmp_write(OutFile, outputImage, width, height);
	delete[] inputImage;
	delete[] outputImage;
	return 0;
}
